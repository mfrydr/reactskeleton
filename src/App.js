import React  from 'react';
//import logo from './logo.svg';
import './App.css';
//import Counter from './components/Counter';
import SushiCard from './components/SushiCard';
function App() {
  const url = "https://sonic.dawsoncollege.qc.ca/~jaya/sushi/assets/nigiri/ebi.webp";
  const description = "A Tiger Shrimp (cooked) topping served on top of sushi rice";
  const alt = "ebi";
  return (
    <div className="App">
      <SushiCard 
        imageURL = {url}
        description = {description}
        alt = {alt}
      />
    </div>
  );
}

export default App;
