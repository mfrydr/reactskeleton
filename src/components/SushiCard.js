import React, { Component } from "react";
import './SushiCard.css';

class SushiCard extends Component {
  render() {
    return (
      <article className="card">
        <figure className="card-thumbnail">
          <img src={this.props.imageURL} alt={this.props.alt} />
        </figure>
        <section className="card-description">
          {this.props.description}
        </section>
      </article>
    );
  }
}

export default SushiCard;
